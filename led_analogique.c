/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */
#define _XTAL_FREQ 20000000L


uint16_t Digit[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f};
//uint16_t value = 314;
 
uint32_t valeur = 0;

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/* i.e. uint8_t <variable_name>; */



/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/


void main(void)
{
    /* Configure the oscillator for the device */
    ConfigureOscillator(); 
    
    
    

    /* Initialize I/O and Peripherals for application */
    InitApp();
    
    /* TODO <INSERT USER APPLICATION CODE HERE> */
   TRISD = 0x00;
   TRISE = 0x00;
   ADFM = 1;
   ADON = 1;
   ADCON2 = 0xBE;

    while(1) //boucle infinie comme on a un syst�me embarqu�

    {
        
        if (GODONE == 0)
        {
            valeur = ADRES; 
            GODONE = 1;
        }
        
        valeur=(valeur*500)/1023;
        
        //1er digit
        LATE = 0b111; 
        LATD = Digit[valeur/100];
        LATD = LATD | 0x80;
        LATE = 0b110;
        __delay_ms(1);
        //2�me digit
        LATE = 0b111;
        LATD = Digit[(valeur/10)%10];
        LATE = 0b101;
        __delay_ms(1);
        //3�me digit
        LATE = 0b111;
        LATD = Digit[valeur%10];
        LATE = 0b011;  
        __delay_ms(1);
       
        /*
        //1er digit
        LATE = 0b111; //permet de ne pas avoir une mini version sur de l'ancien chiffre sur le prochain LATE
        LATD = Digit[value/100];
        LATD = LATD | 0x80;
        LATE = 0b110;
        __delay_ms(1);
        //2�me digit
        LATE = 0b111;
        LATD = Digit[(value/10)%10];
        LATE = 0b101;
        __delay_ms(1);
        //3�me digit
        LATE = 0b111;
        LATD = Digit[value%10];
        LATE = 0b011;
        __delay_ms(1);*/
        
       
         
    }

}


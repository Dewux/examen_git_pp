/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */



/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/
#define _XTAL_FREQ 20000000L
/* i.e. uint8_t <variable_name>; */

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{   

    /* Configure the oscillator for the device */
    ConfigureOscillator();
    TRISD = 0x00;
    TRISE = 0x00;
    /* Initialize I/O and Peripherals for application */
    InitApp();

    /* TODO <INSERT USER APPLICATION CODE HERE> */
    while(1)
    {
     
        LATD = 0x01;
        __delay_ms(100); 
        LATD = 0x02;
        __delay_ms(100); 
        LATD= 0x04;
        __delay_ms(100);     
        LATD = 0x08;
        __delay_ms(100); 
        LATD = 0x10;
        __delay_ms(100); 
        LATD = 0x20;
        __delay_ms(100); 
        LATD = 0x40;
        __delay_ms(100); 
        LATD= 0x80;
        __delay_ms(100); 
        LATD= 0x00;
        
        LATE=0x01;
        __delay_ms(100); 
        LATE=0x02;
        __delay_ms(100); 
        LATE=0x04;
        __delay_ms(100);   
        LATE=0x02;
        __delay_ms(100); 
        LATE=0x01;
        __delay_ms(100); 
         LATE=0x00;
       
        LATD = 0x80;
        __delay_ms(100); 
         LATD = 0x40;
        __delay_ms(100); 
         LATD = 0x20;
        __delay_ms(100);         
        LATD = 0x10;
        __delay_ms(100);        
        LATD = 0x08;
        __delay_ms(100); 
        LATD= 0x04;
        __delay_ms(100);     
        LATD = 0x02;
        __delay_ms(100); 
  

 
      }
    
}





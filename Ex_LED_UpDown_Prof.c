/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/
#define _XTAL_FREQ 20000000L
/* i.e. uint8_t <variable_name>; */

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{
    uint8_t direction = 0;
    uint16_t mesLeds = 1;

    TRISD = 0x00;      // all is output
    TRISE = 0xF8;      // (RE0-RE2) is output

    LATD = mesLeds;       // intial value
    LATE = mesLeds >> 8;  // intial value

    //----------------------------------------------
    while(1)        // loop until doomsday
    {
        if(direction == 0)  // goes up
        {
            __delay_ms(100);
            if(mesLeds < 0x400) // last possible led
            {
                mesLeds = mesLeds << 1;
                LATD = mesLeds;
                LATE = mesLeds >> 8;
            }
            else
            {
                direction = 1;
                mesLeds = mesLeds >> 1;
                LATD = mesLeds;
                LATE = mesLeds >> 8;
            }
        }
        else
        {
            __delay_ms(100);
            if(mesLeds > 0x001)
            {
                mesLeds = mesLeds >> 1;
                LATD = mesLeds;
                LATE = mesLeds >> 8;
            }
            else
            {
                direction = 0;
                mesLeds = mesLeds << 1;
                LATD = mesLeds;
                LATE = mesLeds >> 8;
            }
        }
    }

}


/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */



/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/* i.e. uint8_t <variable_name>; */

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{   


    /* Configure the oscillator for the device */
    ConfigureOscillator();
    TRISD = 0;
    LATD = 1;
    /* Initialize I/O and Peripherals for application */
    InitApp();

    /* TODO <INSERT USER APPLICATION CODE HERE> */

    for(;;) //while (1)
        {
            if (PORTBbits.RB0 ==1)
            {   
                if (LATD !=0x80)
                {
                    LATD =LATD << 1;
                }
                
                while(PORTBbits.RB0 ==1)
                {
                }
            }
            if (PORTBbits.RB1 ==1)
            {   
              if (LATD !=0x01)
                {
                    LATD =LATD >> 1;
                }
                
                while(PORTBbits.RB1 ==1)
                {
                }
            }
           
        }
            
 
    
}
  




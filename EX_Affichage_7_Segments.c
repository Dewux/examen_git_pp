/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/
#define _XTAL_FREQ 20000000L

uint16_t counter;

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/
void interrupt allumage (void)
{
     if(TMR2IF ==1)
        {
            TMR2IF=0;
            Counter ++;
            if (counter==1000)
        }
    
}

void main(void)

{   
    TRISD=0;
    INT0IE=1; 
    INT1IE=1;
    GIE=1;   
}


/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */
#define _XTAL_FREQ 20000000L

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/* i.e. uint8_t <variable_name>; */


/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{
    TRISD = 0;
    LATD = 1;
   
    void interrupt high-isr(void)
    {
        static uint16_t count;
        static uint8_t digSel=0;
        static uint16_t seconds = 0;
        if(TMR2IF == 1)
        {
            TMR2IF = 0;
            count++;
            if((count %5) == 0)
            {
                Digit_display(seconds, digSel);
                digSel = (digSel+1)%3;
            }
            if(count = 1000);
            {
                count = 0;
                seconds++;
            }
           
        }
    }
}


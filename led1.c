/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */
#define _XTAL_FREQ 20000000L

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/* i.e. uint8_t <variable_name>; */
uint8_t iValLedD = 0;
uint8_t iValLedE = 0;


/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/


void main(void)
{
    /* Configure the oscillator for the device */
    ConfigureOscillator();
    
    //TRISB = 0xFF;//pas besoin de le mettre comme par d�faut en entr�e
    

    /* Initialize I/O and Peripherals for application */
    InitApp();
    TRISD = 0x00;
    uint8_t iSens =0;
    uint16_t iLed = 1;
    TRISE = 0xF8;
    LATD = iLed ;
    LATE = iLed>>8; //d�cal� de 8
    //LATE = 1;
    /* TODO <INSERT USER APPLICATION CODE HERE> */

    while(1) //boucle infinie comme on a un syst�me embarqu�

    {
        if (iSens == 0)
        {
          __delay_ms(100);
          if(iLed <0x400) //derni�re led possible
          {
              iLed = iLed << 1;
              LATD = iLed;
              LATE = iLed >> 8;
          }
          else
          {
              iSens=1;
              iLed = iLed >>1;
              LATD = iLed;
              LATE = iLed >> 8;
          }
        }
        else
        {
           __delay_ms(100);
          if(iLed <0x001) //derni�re led possible
          {
              iLed = iLed >> 1;
              LATD = iLed;
              LATE = iLed >> 8;
          }
          else
          {
              iSens=0;
              iLed = iLed <<1;
              LATD = iLed;
              LATE = iLed >> 8;  
           }
        }
        
        
        
        /*if (PORTBbits.RB0 ==1)
        {
            if(LATD !=0x80)
            {
            LATD = LATD <<1;
            }
            if((LATD == 0x80) && (LATE != 0x30))
            {
                LATE=LATE<<1;
                LATD=0x00;
            }
            while(PORTBbits.RB0 == 1)
            {}
        }
         if (PORTBbits.RB1 ==1)
        {
             if (LATE !=0x01)
             {
                 LATE = LATE >>1;
             }
            if( (LATE == 0x01) &&(LATD !=0x01))
            {
            LATD = LATD >>1;
            }
            
            while(PORTBbits.RB1 == 1)
            {}
        }*/
        
    }

}

